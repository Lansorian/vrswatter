﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannotCollide : MonoBehaviour
{
    private GameObject[] feathers;

    private int[] x_range = { -50, 130};
    private int[] y_range = { 20, 75 };
    private int[] z_range = { 100, 185 };

    private bool isCollide = false;

    // Use this for initialization
    void Start()
    {
        feathers = GameObject.FindGameObjectsWithTag("Feather");
    }

    // Update is called once per frame
    void Update()
    {
        CheckPositions();
        if (isCollide)
        {
            RandomPosition();
        }
    }

    private void CheckPositions()
    {
        foreach (GameObject feather1 in feathers)
        {
            if (!feather1.Equals(gameObject) && feather1.transform.position == gameObject.transform.position)
            {
                RandomPosition();
            }
        }
    }

    private void RandomPosition()
    {
        int positionx = Random.Range(x_range[0], x_range[1]);
        int positiony = Random.Range(y_range[0], y_range[1]);
        int positionz = Random.Range(z_range[0], z_range[1]);

        transform.SetPositionAndRotation(new Vector3(positionx, positiony, positionz), transform.rotation);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Kokonut" || other.tag == "Throwable" || other.tag == "Kokoking" || other.tag == "Fireball")
        { }
        else
        {
            isCollide = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        isCollide = false;
    }
}
