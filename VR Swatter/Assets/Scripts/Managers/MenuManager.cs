﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;
using System;

public class MenuManager : MonoBehaviour
{
    enum MenuState { Settings, MainMenu };
    public Animator VRUIAnimator;
    public Text PressXToJoinText;
    private MusicManager musicManager;
    private Text text;
    private int numPlayers = 1;

    public GameObject[] players;
    public GameObject InvertCanvas;
    public GameObject SettingsCanvas;
    public GameObject Leaderboard;
    public GameObject VRLeaderboard;

    private List<GameObject> _activePlayers;

    private float _startGameTimer = 9f;
    private float _startGameSplashTime = 8f;
    private float _startGameDelay = 0f;
    private bool _starting = false;

    private MenuState _currentState = MenuState.MainMenu;

    public GameObject LeftHand;
    public GameObject RightHand;

    // Use this for initialization
    void Awake()
    {
        musicManager = GetComponent<MusicManager>();
        SettingsCanvasManager settingsCanvas = SettingsCanvas.GetComponent<SettingsCanvasManager>();
        settingsCanvas.OnSettingsExit += new SettingsCanvasManager.exitMenuClicked(CloseSettings);
        GameSettings.UpdateLeaderboardDisplay(VRLeaderboard);
        text = GetComponent<Text>();
    }

    void Update()
    {
        switch (_currentState)
        {
            case MenuState.MainMenu:
                {
                    MenuUpdate();
                    return;
                }
            case MenuState.Settings:
                {
                    SettingsUpdate();
                    return;
                }
            default:
                {
                    // TODO: this should not be reached, maybe throw error
                    return;
                }
        }
    }

    private void SettingsUpdate()
    {
        var oButtonPressed = Input.GetButtonDown("P0_O");
        var exitSettingsMenu = Input.GetButtonDown("Share") || oButtonPressed;
        if (exitSettingsMenu)
        {
            CloseSettings();
            return;
        }

        _activePlayers.ForEach(player => Kokonut.Disable(player));
        SettingsCanvas.SetActive(true);

    }

    private void CloseSettings()
    {
        SettingsCanvas.GetComponent<SettingsCanvasManager>().ResetCanvas();
        SettingsCanvas.SetActive(false);
        _activePlayers.ForEach(player => Kokonut.Disable(player, false));
        _currentState = MenuState.MainMenu;
    }

    private void MenuUpdate()
    {
        if (LeftHand != null)
        {
            try
            {
                GameObject handL = LeftHand.transform.Find("LeftRenderModel Slim(Clone)/controller(Clone)").gameObject;
                if (handL != null)
                {
                    handL.SetActive(false);
                }
            }
            catch
            {
                print("Left hand not found");
            }
        }
        if (RightHand != null)
        {
            try
            {
                GameObject handR = RightHand.transform.Find("RightRenderModel Slim(Clone)/controller(Clone)").gameObject;
                if (handR != null)
                {
                    handR.SetActive(false);
                }
            }
            catch
            {
                print("Right hand not found");
            }
        }

        GameSettings.UpdateLeaderboardDisplay(VRLeaderboard);
        _activePlayers = players.Where(player => player.activeInHierarchy).ToList();
        _activePlayers.ForEach(player =>
        {
            var motion = player.GetComponent<Motion>();
            if (motion.enabled == false)
            {
                motion.enabled = true;
                motion.moveSpeed = 5;
            }
        });

        for (int i = 1; i < 5; i++)
        {
            if (Input.GetButtonDown("P" + i + "_X"))
            {
                // If set controller list doesn't contain current controller
                // Find first inactive Kokonut and assign controller to current controller
                if (!GameSettings.ControllerSet.ContainsKey(i))
                {
                    for (int j = 0; j < players.Length; j++)
                    {
                        if (!players[j].activeSelf)
                        {
                            var kokonut = players[j].GetComponent<Kokonut>();
                            GameSettings.ControllerSet.Add(i, kokonut.GetID());
                            players[j].SetActive(true);
                            kokonut.SetControllerID(i);
                            kokonut.PlayLaugh();
                            break;
                        }
                    }
                }
            }
            if (!_starting)
            {
                if (Input.GetButtonDown("P" + i + "_Tri"))
                {
                    if (GameSettings.ControllerSet.ContainsKey(i))
                    {
                        GameSettings.Invert["P" + i] = !GameSettings.Invert["P" + i];

                        if (GameSettings.ControllerSet[i] == "P1")
                        {
                            GameSettings.Invert["P0"] = !GameSettings.Invert["P0"];
                        }

                        if (GameSettings.Invert["P" + i])
                        {
                            InvertCanvas.transform.Find(GameSettings.ControllerSet[i] + "Invert").GetComponent<Text>().text = "Inverted!";
                        }
                        else
                        {
                            InvertCanvas.transform.Find(GameSettings.ControllerSet[i] + "Invert").GetComponent<Text>().text = "";
                        }
                    }
                }
            }
        }

        numPlayers = _activePlayers.Count;
        GameSettings.numPlayers = numPlayers;

        if (numPlayers > 0)
        {
            if (numPlayers == 4)
            {
                PressXToJoinText.gameObject.SetActive(false);
            }

            if (!_starting)
            {
                text.text = "Press       to Begin!";
                transform.Find("Options").gameObject.SetActive(true);
                if (Input.GetButtonDown("Submit"))
                {
                    _starting = true;
                }
            }
            else
            {
                _startGameTimer -= Time.deltaTime;
                PressXToJoinText.gameObject.SetActive(false);
                text.text = "Starting!";
                transform.Find("Options").gameObject.SetActive(false);
                musicManager.fadeOut();
                VRUIAnimator.SetTrigger("FadeIn");
                if (_startGameTimer <= _startGameSplashTime)
                {
                    InvertCanvas.transform.Find("Objective").gameObject.SetActive(true);
                }
                var preRoundTimerOver = _startGameTimer <= _startGameDelay;
                if (preRoundTimerOver)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                }
            }
        }

        if (!_starting)
        {
            if (Input.GetButtonDown("Share"))
            {
                _currentState = MenuState.Settings;
            }
            if (Input.GetButtonDown("Cancel"))
            {
                if (Leaderboard.activeSelf)
                {
                    Leaderboard.SetActive(false);
                }
                else
                {
                    Leaderboard.SetActive(true);
                    GameSettings.UpdateLeaderboardDisplay(Leaderboard);
                }
            }

            if (Leaderboard.activeSelf)
            {
                if (Input.GetButtonDown("P0_O") && Input.GetButtonDown("P0_SQ"))
                {
                    GameSettings.ClearLeaderboard();
                    GameSettings.UpdateLeaderboardDisplay(Leaderboard);
                    GameSettings.UpdateLeaderboardDisplay(VRLeaderboard);
                }
            }
        }
    }
}
