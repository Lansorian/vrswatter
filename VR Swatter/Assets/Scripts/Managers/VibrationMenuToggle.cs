﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VibrationMenuToggle : MonoBehaviour {

    public Color ActiveSettingColor;
    public Color InactiveSettingColor;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (GameSettings.VRVibration)
        {
            transform.Find("On").gameObject.GetComponent<Image>().color = ActiveSettingColor;
            transform.Find("Off").gameObject.GetComponent<Image>().color = InactiveSettingColor;
        }
        else
        {
            transform.Find("Off").gameObject.GetComponent<Image>().color = ActiveSettingColor;
            transform.Find("On").gameObject.GetComponent<Image>().color = InactiveSettingColor;
        }
	}
}
