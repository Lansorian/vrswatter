﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

public class ThrowablePhysics : MonoBehaviour
{
    public GameObject Display;
    private int SmackPoints = 100;
    public float SmackPower;
    private ConstantForce gravity;
    private Rigidbody rb;
    private List<GameObject> _activePlayers;

    // Use this for initialization
    void Start()
    {
        Physics.IgnoreLayerCollision(gameObject.layer, 16, true);
        Physics.IgnoreLayerCollision(gameObject.layer, gameObject.layer, true);
        gravity = gameObject.AddComponent<ConstantForce>();
        gravity.force = new Vector3(0.0f, -150f, 0.0f);
        rb = gameObject.GetComponent<Rigidbody>();
        rb.useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Floor" || collision.gameObject.tag == "Collider")
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            Destroy(gameObject, 1.5f);
        }

        if (collision.gameObject.tag == "Kokonut")
        {
            if (collision.relativeVelocity.magnitude > 15)
            {
                collision.rigidbody.AddForce(rb.velocity * SmackPower);
                Kokonut kokonut = collision.gameObject.GetComponent<Kokonut>();
                if (!kokonut.IsSmacked())
                {
                    kokonut.GotSmacked();
                    GameObject ScoreDisplay = Instantiate(Display, kokonut.transform.position, Quaternion.identity);
                    Kokonut[] inLead = GetWinningKokonuts();
                    if (inLead.Contains(kokonut) && kokonut.GetNumFeathers() > 0)
                    {
                        if (inLead.Length == 1)
                        {
                            SmackPoints += SmackPoints / 2;
                        }
                        else
                        {
                            SmackPoints += SmackPoints / 4;
                        }
                    }
                    ScoreDisplay.transform.Find("Canvas/Score").gameObject.GetComponent<Text>().text = "+" + SmackPoints.ToString();
                    ScoreDisplay.GetComponent<Animator>().SetTrigger("ScoreDisplay");
                    ScoreManager.UpdateScore(SmackPoints);
                    kokonut.StartDisabledTimer();
                }
            }
        }
    }

    private Kokonut[] GetWinningKokonuts()
    {
        _activePlayers = GameObject.FindGameObjectsWithTag("Kokonut").Where(player => player.activeInHierarchy).ToList();
        try
        {
            var topScore = _activePlayers
                .Select(player => player.GetComponent<Kokonut>())
                .Select(kokonut => kokonut.GetNumFeathers())
                .Max();
            var winners = _activePlayers
                .Select(player => player.GetComponent<Kokonut>())
                .Where(kokonut => kokonut.GetNumFeathers() == topScore)
                .ToArray();
            return winners;
        }
        catch
        {
            return new Kokonut[] { };
        }
    }
}
