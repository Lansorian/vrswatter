﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class EnterName : MonoBehaviour {

    private string _name = "";
    private int _maxNameLength = 5;

    public Text nameDisplay;
    public GameObject pointer;
    public GameManager manager;
    private GameObject KeyButton;
    private string character = "";

    [SteamVR_DefaultAction("Interact")]
    public SteamVR_Action_Boolean trigger;

    SteamVR_Behaviour_Pose trackedObj;

    // Use this for initialization
    void Start () {
        trackedObj = GetComponent<SteamVR_Behaviour_Pose>();
        pointer.transform.localPosition = new Vector3(pointer.transform.localPosition.x, -20f, 26);
        pointer.transform.localScale = new Vector3(pointer.transform.localScale.x, 0.5f, pointer.transform.localScale.z);
	}
	
	// Update is called once per frame
	void Update () {
        if (trigger.GetStateDown(trackedObj.inputSource))
        {
            character = pointer.GetComponent<PointerCollision>().character;
            if (character != "")
            {
                if (character == "Backspace")
                {
                    if (_name.Length > 0)
                    {
                        _name = _name.Substring(0, _name.Length - 1);
                    }
                }
                else if (character == "Enter")
                {
                    if (_name.Length > 0 && _name.Length <= _maxNameLength)
                    {
                        GameSettings.UpdateLeaderboard(_name, ScoreManager.GetKokoKingScore());
                        GameSettings.Save();
                        manager.DoneKeyboard(true);
                    }
                }
                else
                {
                    if (_name.Length < _maxNameLength)
                    {
                        _name += character;
                    }
                }
            }
            if (_name.Length == 0)
            {
                nameDisplay.text = "__ __ __ __ __";
            }
            else if (_name.Length == 1)
            {
                nameDisplay.text = _name + " __ __ __ __";
            }
            else if (_name.Length == 2)
            {
                nameDisplay.text = _name + " __ __ __";
            }
            else if (_name.Length == 3)
            {
                nameDisplay.text = _name + " __ __";
            }
            else if (_name.Length == 4)
            {
                nameDisplay.text = _name + " __";
            }
            else if (_name.Length == 5)
            {
                nameDisplay.text = _name;
            }
        }
	}

    public string GetName()
    {
        return _name;
    }
}
